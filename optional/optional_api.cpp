#include <array>
#include <optional>
#include <print>
#include <span>
#include <string>

namespace // Stubs
{

// Stub
int read_bytes(void *data, size_t size) { return 0; }

}


struct otp_factory_data_t { /* … */ };


namespace // API with std::optional
{

// - Unclear ownership
// - No way to return error values
// + Ease of use
otp_factory_data_t *
get_factory_data_old1()
{
	static otp_factory_data_t factory_data;
	const int res = read_bytes(&factory_data, sizeof(factory_data));
	if(res != 0)
		return NULL;
	return &factory_data;
}

// + Clear ownership
// + Can return error values (by changing to int)
// - Not great to use
bool
get_factory_data_old2(otp_factory_data_t *factory_data)
{
	if(!factory_data)
		return false;
	const int res = read_bytes(factory_data, sizeof(*factory_data));
	return res != 0;
}


// + Clear ownership
// o Can return error values (by changing to std::expected)
// - Ease of use
std::optional<otp_factory_data_t>
get_factory_data_new()
{
	otp_factory_data_t factory_data;
	const int res = read_bytes(&factory_data, sizeof(factory_data));
	if(res != 0)
		return {};
	return factory_data;
}

}

namespace // API with std::span and std::optional
{

enum setting_id_t
{
	setting_id_first = 0,
	setting_id_max = 9,
};

bool
get_large_setting_old(setting_id_t id, const uint8_t **value, size_t *size)
{
	struct setting_t
	{
		uint8_t		value[16];
		size_t		length{};
	};
	static setting_t settings[10] = { /* … */};

	if(id > setting_id_max || !value || !size)
		return false;

	*value = settings[id].value;
	*size = settings[id].length;
	return true;
}

std::optional<std::span<uint8_t>>
get_large_setting_new(setting_id_t id)
{
	struct setting_t
	{
		std::array<uint8_t, 16>		value;
		size_t						length{};
	};
	static setting_t settings[10] = { /* … */};

	if(id > setting_id_max)
		return std::nullopt;

	return std::span<uint8_t>{ settings[id].value.begin(), settings[id].length };
}

}

int
main()
{
	// Old 1
	{
		const otp_factory_data_t * const factory_data = get_factory_data_old1();
		if(factory_data)
		{
			// do_stuff_with(*factory_data);
		}
		// Need to 'free(factory_data)'? Must check source/doc.
	}

	// Old 2
	{
		otp_factory_data_t factory_data;	// We own the data (but can't make it const)
		if(get_factory_data_old2(&factory_data))
		{
			// do_stuff_with(factory_data);
		}
	}

	// New
	{
		// Data owned by us and const
		const auto factory_data = get_factory_data_new();
		if(factory_data)
		{
			// do_stuff_with(*factory_data);
		}
	}

	return 0;
}
