#include <functional>
#include <memory>
#include <optional>
#include <print>
#include <string>
#include <tuple>


// Helper to allow formatting std::optional values with std::print() and friends.
template<typename T>
struct std::formatter<std::optional<T>>
{
	template<typename ParseContext>
	constexpr auto
	parse(ParseContext& ctx) { return ctx.begin(); }

	template<typename FmtContext>
	FmtContext::iterator
	format(const std::optional<T>& opt, FmtContext& ctx) const
	{
		auto it = ctx.out();
		if(opt.has_value())
			return std::format_to(it, "{}", opt.value());
		return std::format_to(it, "<?>");
	}
};


std::optional<std::string>
find_holy_grail(bool lucky)
{
	if(lucky)
		return "🏆";
	return std::nullopt;	// or: return {}
}

std::optional<int>
divide(int dividend, int divisor)
{
	if(divisor == 0)
		return {};
	return dividend / divisor;
}

// Returns the first non-empty token of a space-delimited string
std::optional<std::string_view>
get_token(std::string_view s)
{
	const char Delim = ' ';
	if(s.empty() || s.starts_with(Delim))
		return {};
	const auto pos = s.find(Delim);
	if(pos != std::string_view::npos)
		return s.substr(0, pos);
	return s;
}


int
main()
{
	if(const auto result = find_holy_grail(true);
	   result)	// or: result.has_value()
	{
		std::println("Yay! Here it is: {}", *result);	// or: result.value()
	}
	else
	{
		std::println("Too bad");
	}

	std::println("lucky => {}", find_holy_grail(true).value_or("😢"));
	std::println("unlucky => {}", find_holy_grail(false).value_or("😢"));

	//
	// C++23 monadic operations

	// Transform while preserving type
	{
		const auto times_100 = [] (int x) -> int { return x * 100; };
		std::println("42 / 7 * 100 = {}", divide(42, 7).transform(times_100));
		std::println("42 / 0 * 100 = {}", divide(42, 0).transform(times_100));
	}

	// Transform int → std::string using a lambda
	const auto int_to_string = [] (int x) { return std::format("{}", x); };
	std::println(R"(42 / 7 * 100 → stringify = "{}")",
	             divide(42, 7).transform(int_to_string));

	// Transform int → std::string using a function pointer to a specific std::to_string() overload
	std::println(R"(42 / 7 * 100 → stringify = "{}")",
	             divide(42, 7).transform(static_cast<std::string(*)(int)>(std::to_string)));

	// More elaborate transformations (just because we can)
	std::println("(42 / 7) → wrap → double → unwrap → value_or(-1) = {}",
	             divide(42, 7)
	                 .transform(std::make_unique<int, int>)                               // int → unique_ptr<int>
	                 .transform([] (std::unique_ptr<int>&& pi) { *pi *= 2; return pi; })  // unique_ptr<int> → unique_ptr<int>
	                 .transform([] (const std::unique_ptr<int>& pi) { return *pi; })      // unique_ptr<int> → int
	                 .value_or(-1)                                                        // int → int
	);
	std::println("(42 / 0) → wrap → double → unwrap → value_or(-1) = {}",
	             divide(42, 0)
	                 .transform(std::make_unique<int, int>)
	                 .transform([] (std::unique_ptr<int>&& pi) { *pi *= 2; return pi; })
	                 .transform([] (const std::unique_ptr<int>& pi) { return *pi; })
	                 .value_or(-1)
	);
	std::println("Result of (42 / 7) is stored at {}",	// Note: Pointer will be dangling after statement completes.
	             (void *)divide(42, 7)
	                 .transform(std::make_unique<int, int>)    // int → unique_ptr<int>
	                 .transform(&std::unique_ptr<int>::get)    // unique_ptr<int> → int *
	                 .value()
	);

	// Operation chaining with and_then() while preserving type
	const auto prettify = [] (const std::string& s) -> std::optional<std::string> {	// or (auto&& s)
		return std::format("✨{}✨", s);
	};
	const auto uglify = [] (const std::string& s) -> std::optional<std::string> {	// or (auto&& s)
		return std::format("💩{}🪰", s);
	};
	std::println("(42 / 7) → prettify → uglify = {}", divide(42, 7).transform(int_to_string).and_then(prettify).and_then(uglify));
	std::println("(42 / 7) → uglify → prettify = {}", divide(42, 7).transform(int_to_string).and_then(uglify).and_then(prettify));
	std::println("(42 / 0) → prettify → uglify = {}", divide(42, 0).transform(int_to_string).and_then(prettify).and_then(uglify));

	// Operation chaining with and_then() while changing the type
	const auto parse_int = [] (std::string_view s) -> std::optional<int> {
		try
		{
			auto pos = size_t{};
			const auto i = std::stoi(std::string{ s }, &pos);
			if(pos != s.size())
				return {};
			return i;
		}
		catch(std::exception)
		{
			return {};
		}
	};
	const auto filter_odd = [] (int x) {
		return x % 2 == 0 ? std::make_optional(x) : std::nullopt;	// or: std::optional<int>{ x }
	};
	for(const auto& s : { "42 bar", "17 bar", "3.14 bar", "foo bar" })
	{
		std::println("{} → {}",
		             s,
		             get_token(s)
		                 .and_then(parse_int)
		                 .and_then(filter_odd)
		                 .transform(int_to_string)
		                 .and_then(prettify)
		);
	}

	return 0;
}
