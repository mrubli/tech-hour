# C++ Coroutines

## Prerequisites

You'll need [Meson](https://mesonbuild.com/Getting-meson.html) 1.3 or newer for this.
A the time of writing, Debian Trixie only comes with Meson 1.2.3 but you can install it using PIP:

```
apt remove meson
pip install --break-system-packages meson
```

## Build instructions

```
meson setup builddir/
ninja -C builddir/
```
