#include <generator>
#include <print>


std::generator<int>
generate_odds(int until)
{
	for(int i = 1; i <= until; i += 2)
	{
		co_yield i;
	}
}


int
main()
{
	for(auto i : generate_odds(13))
	{
		std::println("{}", i);
	}

	return 0;
}
