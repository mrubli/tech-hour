// Simple asymmetric communication protocol demo using cppcoro.
//
// The communication looks like this:
//
//   Client                                       Server
//    |<------------------ "welcome" -----------------|
//    |------------------ "this is x" --------------->|
//    |<------------ "hello x. this is y."------------|
//    |------------------- "hello y" ---------------->|
//    |<-------------------- "bye" -------------------|
//    |------------------- "see you" ---------------->|

#include <print>
#include <stdexcept>
#include <string_view>

#include <cppcoro/generator.hpp>
#include <cppcoro/schedule_on.hpp>
#include <cppcoro/single_consumer_event.hpp>
#include <cppcoro/static_thread_pool.hpp>
#include <cppcoro/sync_wait.hpp>
#include <cppcoro/task.hpp>
#include <cppcoro/when_all.hpp>


struct pipe_t
{
	std::string_view				tag_left;		// For logging only
	std::string_view				tag_right;		// For logging only
	std::string						buffer;			// Packet data
	cppcoro::single_consumer_event	data_ready;		// Synchronization
};

auto s2c = pipe_t{ " <--", "" };	// Server-to-client
auto c2s = pipe_t{ "", "--> " };	// Client-to-server


//
// Helper functions

void
print(const pipe_t& pipe, std::string_view data, bool is_server_side)
{
	const auto traffic = std::format("{} \"{}\" {}", pipe.tag_left, data, pipe.tag_right);
	if(is_server_side)
		std::println("{: <35}{: >35} [Server]", "", traffic);
	else
		std::println("[Client] {}", traffic);
}

cppcoro::task<std::string>
receive(pipe_t& pipe, bool is_server_side)
{
	co_await pipe.data_ready;
	pipe.data_ready.reset();	// Reset for next time. Events remain signaled until reset.

	const auto data = std::exchange(pipe.buffer, {});
	print(pipe, data, is_server_side);
	co_return data;
}

void
send(std::string_view data, pipe_t& pipe, bool is_server_side)
{
	pipe.buffer = data;
	print(pipe, data, is_server_side);
	pipe.data_ready.set();
}


//
// Client

cppcoro::task<void>
client_handle_connection(cppcoro::static_thread_pool& tp)
{
	std::println("Client started");

	auto& in = s2c;
	auto& out = c2s;

	co_await receive(in, false);				// "welcome"
	send("this is x", out, false);

	co_await receive(in, false);				// "hello x. this is y."
	send("hello y", out, false);

	co_await receive(in, false);				// "bye"
	send("see you", out, false);

	std::println("Client done");
}


//
// Server

cppcoro::task<void>
server_handle_connection(cppcoro::static_thread_pool& tp)
{
	std::println("Server started");

	auto& in = c2s;
	auto& out = s2c;

	send("welcome", out, true);
	co_await receive(in, true);					// "this is x"
	send("hello x. this is y.", out, true);
	co_await receive(in, true);					// "hello y"
	send("bye", out, true);
	co_await receive(in, true);					// "see you"

	std::println("Server done");
}


int
main()
{
	cppcoro::static_thread_pool tp;			// std::thread::hardware_concurrency() threads
	//cppcoro::static_thread_pool tp(1);	// Single thread

	auto server = server_handle_connection(tp);
	auto client = client_handle_connection(tp);
	auto all_ready = cppcoro::when_all(
		cppcoro::schedule_on(tp, std::move(server)),
		cppcoro::schedule_on(tp, std::move(client))
	);
	cppcoro::sync_wait(all_ready);
}
