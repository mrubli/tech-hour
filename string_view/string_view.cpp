#include <print>
#include <string>
#include <string_view>


using namespace std::literals;	// Import string literals that allow writing "foo"s and "foo"sv


void
print(std::string_view sv)		// std::string_view is a value type (cheap to copy)
{
	std::println("{}", sv);
}


void
analyze(std::string_view sv)
{
	// size() does not include any null-terminators
	std::println(R"("{}" is {} characters long.)",
	             sv,
	             sv.size());

	// contains() and starts_with()
	std::println(R"("{}" {} an 's'.)",
	             sv,
	             sv.contains('s') ? "contains" : "does not contain");
	std::println(R"("{}" {} "I'm".)",
	             sv,
	             sv.starts_with("I'm") ? "starts with" : "does not start with");

	// "Modifying" a string_view. For example:
	//
	//     0   1   2   3   4   5   6   7   8   9  10  11  12  13  14
	//   +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
	//   | I | ' | m |   | a |   | C |   | s | t | r | i | n | g | ␀ | 15 bytes
	//   +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
	//
	//   <------------------ string_view before ----------------->
	//   <--- prefix ---><---------- string_view after ---------->
	//
	// Note how this works even if the represented string is const. We only modify the _view_.
	constexpr auto PrefixLength = 4;
	sv.remove_prefix(PrefixLength);
	std::println(R"(Our string without the first {} characters: "{}")",
	             PrefixLength,
	             sv);

	// Comparison
	std::println(R"("{}" is {} to "a C string")",
	             sv,
	             sv == "a C string" ? "equal" : "NOT equal");

	// Looping
	std::print(R"("{}" consists of these characters: [)", sv);
	for(int i = 0; const char c : sv)
	{
		std::print("{} '{}'",
		           i++ > 0 ? "," : "",
		           c);
	}
	std::println(" ]");
}


int
main()
{
	// Various ways to construct a std::string_view
	print("I'm a const char * from the days of yore.");

	print(std::string{ "I'm a std::string" });
	print("I'm also a std::string but constructed in a cooler way"s);

	print(std::string_view{ "I'm a std::string_view, representing a const char *" });
	print("I'm a std::string_view, representing a const char * but constructed in a cooler way"sv);

	// String comparison
	// Note: R"(…)" is a raw string and allows … to contain quotes.
	std::println("---");
	{
		constexpr const char *a = "foo";
		constexpr char b[] = "foo";
		static_assert(std::string_view{ a } == std::string_view{ b });
		std::println(R"(Comparing two C-style strings:             "{}"@{} is {} to "{}"@{})",
		             a, (void *)a,
		             a == b ? "equal" : "NOT equal",
		             b, (void *)b);
		std::println(R"(Comparing the two string_view equivalents: "{}" is {} to "{}")",
		             a,
		             std::string_view{ a } == std::string_view{ b } ? "equal" : "NOT equal",
		             b);
	}

	// Fun stuff with std::string_view
	std::println("---");
	const char Platitude[] = "I'm a C string";
	analyze(Platitude);

	// A std::string_view will point to anything!
	std::println("---");
	{
		// Create an evil std::string that includes the null-terminator (this is normally a bug)
		const auto s = std::string{ Platitude, sizeof(Platitude) };
		analyze(s);
	}
	std::println("---");
	{
		//                           01234 5   6   7   8   9
		const auto s = std::string{ "Good?\0\xf0\x9f\x98\x88", 10 };
		analyze(s);
	}

	// Careful with dangling references!
	std::println("---");
	{
		const auto greetings = [] (const char *who) {
			const std::string s = std::format("Hello {}!", who);
			//*
			return s;						// Good: Returns an owning std::string
			/*/
			return std::string_view{ s };	// BAD: Returns a view of a local std::string
			//*/
		};
		std::println("{}", greetings("world"));
	}

	return 0;
}
