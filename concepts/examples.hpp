#include <array>
#include <span>


// Creates a std::array from a std::span with static extent.
template<typename T, size_t N>
requires (N != std::dynamic_extent)
auto
to_array(std::span<T, N> c)
{
	auto a = std::array<std::remove_cv_t<T>, N>{};
	std::copy(std::cbegin(c), std::cend(c), std::begin(a));
	return a;
}

// Creates a std::array from a std::span with dynamic extent.
// Note that the ArraySize parameter needs to be explicitly given:
//   to_array<3>(...)
template<size_t ArraySize, typename T, size_t N>
requires (N == std::dynamic_extent)
auto
to_array(std::span<T, N> c)
{
	auto a = std::array<std::remove_cv_t<T>, ArraySize>{};
	std::copy(std::cbegin(c), std::cend(c), std::begin(a));
	return a;
}


// Casts a std::span<T> into a std::span<U>. T and U must be of the same size.
template<typename U, typename T, size_t N>
requires (sizeof(U) == sizeof(T) && std::is_trivial_v<T> && std::is_trivial_v<U>)
std::span<U, N>
span_cast(std::span<T, N> c)
{
	return std::span<U, N>{
		reinterpret_cast<U *>(c.data()),
		c.size()
	};
}
