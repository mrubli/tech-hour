#include <print>

template<typename T>
concept Multipliable = requires(T t) {
	// t * t must be a valid expression. (This could be omitted because it is implied in the
	// requirement below.)
	t * t;
	// The result of t * t must be of the same type as T.
	{ t * t } -> std::same_as<T>;
};

template<typename T>
T square(T value)
requires Multipliable<T>
{
	return value * value;
}

int main()
{
	std::print("{}\n", square(4));
	std::print("{}\n", square(6.28));

	// The square() call below gives an error along these lines:
	//   Constraints not satisfied
	//   In substitution of ‘template<class T> T square(T) requires Multipliable<T> [with T = const char*]’:
	//std::print("{}\n", square("abc"));
	return 0;
}
