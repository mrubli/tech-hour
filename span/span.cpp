#include <algorithm>
#include <array>
#include <print>
#include <random>
#include <span>
#include <string_view>
#include <vector>


// std::span is a value type (cheap to copy)
void
hexdump(std::span<const uint8_t> bytes, std::string_view prefix = {})
{
	std::print("{}[", prefix);
	for(int i = 0; const auto b : bytes)
	{
		std::print("{} {:#04x}", i++ > 0 ? "," : "", b);
	}
	std::println(" ] (size: {})", bytes.size());
}


void
hexdump_and_shuffle(std::span<uint8_t> bytes)
{
	hexdump(bytes, "Before: ");			// Can pass a non-const span as a const span

	auto rd = std::random_device{};
	auto rng = std::mt19937{ rd() };	// Seed the RNG
	std::shuffle(std::begin(bytes), std::end(bytes), rng);
}


void
hexdump_and_zero_last_half(std::span<uint8_t> bytes)
{
	hexdump(bytes, "Before: ");

	// Create a subspan to act only on part of the original container
	const std::span<uint8_t> last_half = bytes.last(bytes.size() / 2);
	std::fill(std::begin(last_half), std::end(last_half), 0);
}


void
hexdump_eight_ints(std::span<const int, 8> elements)
{
	std::print("[");

	static_assert(elements.size() == 8);
	for(auto i = 0; i < 8; i++)
	{
		std::print("{} {}", i > 0 ? "," : "", elements[i]);
	}
	std::println(" ] (size: {})", elements.size());
}


int
main()
{
	// Read-only operation on constant container
	{
		const auto cv = std::vector<uint8_t>{ 0xde, 0xad, 0xbe, 0xef };	// Not vegan Tuesday anymore
		hexdump(cv);
		//hexdump_and_shuffle(cv);	// Would not work. Can't pass a const vector as a non-const span.
	}

	// Read/write operation on non-constant containers
	std::println("---");
	{
		auto v = std::vector<uint8_t>{ 0xde, 0xad, 0xbe, 0xef };
		hexdump_and_shuffle(v);
		hexdump(v, "After:  ");
	}
	std::println("---");
	{
		auto v = std::vector<uint8_t>{ 0x11, 0x22, 0x33, 0x44, 0x55, 0x66 };
		hexdump_and_zero_last_half(v);
		hexdump(v, "After:  ");

		auto a = std::array<uint8_t, 6>{ 0x11, 0x22, 0x33, 0x44, 0x55, 0x66 };
		hexdump_and_zero_last_half(a);
		hexdump(a, "After:  ");
	}

	// Spans with static extent (size known at compile-time)
	std::println("---");
	{
		const auto a8 = std::array{ 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88 };
		hexdump_eight_ints(a8);

		const auto a3 = std::array{ 0x11, 0x22, 0x33 };
		//hexdump_eight_ints(a3);		// Compile-time error because of size mismatch

		const auto v8 = std::vector{ 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88 };
		//hexdump_eight_ints(v8);		// Compile-time error because of unknown vector size

		const int ca8[8] = {};			// Zero-initialize
		hexdump_eight_ints(ca8);

		const int ca16[16] = {};
		//hexdump_eight_ints(ca16);		// Compile-time error because of size mismatch
	}

	return 0;
}
