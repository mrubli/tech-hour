#include <cstdio>
#include <functional>

//using SignumType = std::function<int(int)>;

//int signum_standalone(int x);

void print_signum0(int x)
{
	const auto sgn_func = [] (int value) {
		if (value < 0)
			return -1;
		if (value > 0)
			return 1;
		return 0;
	};
	// Equivalent: const auto sgn_func2 = signum_standalone;
	std::printf("sgn(%d) = %d\n", x, sgn_func(x));
}

void print_signum1(int x)
{
	const int sgn = [] (int value) -> int {
		if (value < 0)
			return -1;
		if (value > 0)
			return 1;
		return 0;
	}(x);
	// Equivalent: const auto sgn2 = signum_standalone(x);
	std::printf("sgn(%d) = %d\n", x, sgn);
}

void print_signum2(int x)
{
	const auto sgn = [x] {
		if (x < 0)
			return -1;
		if (x > 0)
			return 1;
		return 0;
	}();
	std::printf("sgn(%d) = %d\n", x, sgn);
}

struct signum
{
	int operator()(int x) const
	{
		if (x < 0)
			return -1;
		if (x > 0)
			return 1;
		return 0;
	}
};

int main()
{
	print_signum0(42);
	print_signum0(-42);
	print_signum0(0);

	print_signum1(13);
	print_signum1(-13);
	print_signum1(0);

	print_signum2(17);
	print_signum2(-17);
	print_signum2(0);

	std::printf("sgn(%d) = %d\n", 11, signum{}(11));
	std::printf("sgn(%d) = %d\n", -11, signum{}(-11));
	std::printf("sgn(%d) = %d\n", 0, signum{}(0));

	{
		const auto yesnoify = [] (bool b) {
			return b ? "yes" : "no";
		};
		std::printf("true  ⇒ %s\n", yesnoify(true));
		std::printf("false ⇒ %s\n", yesnoify(false));
	}

	return 0;
}
