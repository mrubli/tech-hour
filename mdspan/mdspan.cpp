// Basic examples for std::span and related types.
//
// This requires a relatively new compiler and standard library, e.g. Clang 17, ideally Clang 19.
// Compile as follows:
//   clang++ -std=c++23 -stdlib=libc++ -o mdspan mdspan.cpp

#include <array>
#include <mdspan>
#include <print>
#include <numeric>
#include <ranges>
#include <tuple>
#include <type_traits>
#include <typeinfo>
#include <vector>

#include <cxxabi.h>		// GCC/Clang only


//
// Generic helpers

template<typename, template<typename...> typename>
inline constexpr bool is_specialization = false;

template<template<typename...> typename T, typename... Args>
inline constexpr bool is_specialization<T<Args...>, T> = true;


// Returns the demangled type name of the given argument.
std::string
typename_of(auto x)
{
	// GCC/Clang-only implementation
	return abi::__cxa_demangle(typeid(decltype(x)).name(), NULL, NULL, NULL);
}


// Returns the total size of a (possibly multidimensional) C-style array
template<typename T, std::size_t N>
constexpr
std::size_t
md_array_size(T const(&)[N])
{
    if constexpr(std::is_fundamental_v<T>)
		return N;
    else
		return N * md_array_size(T{});
}


//
// Array helpers

template<typename T>
void
print_array(T x, int field_width, int)
requires std::is_integral_v<T>
{
	std::print(" {:>{}},", x, field_width);
}

// Pretty-prints an N-dimensional array
template<typename T, std::size_t N>
void
print_array(T const(&a)[N], int field_width = 0, int cur_rank = 0)
{
	const auto is_innermost = (std::rank_v<T[N]> == 1);
	const auto indent = std::format("{:>{}}", "", cur_rank);

	std::print("{}[{}", indent, is_innermost ? "" : "\n");
	for(auto i = 0; i < N; i++)
	{
		print_array(a[i], field_width, cur_rank + 1);
	}
	std::println("{}],", is_innermost ? " " : indent);
}


//
// mdspan helpers

template<typename T>
concept MDSpan = is_specialization<T, std::mdspan>;


// Function to match the "exposition-only" fwd-prod-of-extents helper.
// See: https://en.cppreference.com/w/cpp/container/mdspan/extents/fwd-prod-of-extents
// Note: The default argument is an extension by yours truly.
template<typename Extents>
constexpr
typename Extents::size_type
fwd_prod_of_extents(Extents e, typename Extents::rank_type i = Extents::rank())
{
	auto prod = typename Extents::size_type{ 1 };
	for(typename Extents::rank_type rank = 0; rank < i; rank++)
		prod *= e.extent(rank);
	return prod;
}


std::string
extent_to_string(auto e)
{
	return e == std::dynamic_extent ? "?" : std::format("{}", e);
}


// Prints the attributes of a std::mdspan
void
describe(MDSpan auto mds)
{
	std::println("type: {}", typename_of(mds));
	std::print("rank {} mdspan with extents [", mds.rank());
	for(auto rank = 0; rank < mds.rank(); rank++)
	{
		std::print(" {},", mds.extent(rank));
	}
	std::print(" ], static extents: [");
	for(auto rank = 0; rank < mds.rank(); rank++)
	{
		std::print(" {},", extent_to_string(mds.static_extent(rank)));
	}
	std::print(" ], strides: [");
	for(auto rank = 0; rank < mds.rank(); rank++)
	{
		std::print(" {},", mds.stride(rank));
	}
	std::println(" ]");
}


// Prints the contents of a std::mdspan
void
print(MDSpan auto mds, auto... indices)
{
	constexpr auto FieldWidth = 3;
	if constexpr(sizeof...(indices) == mds.rank())
	{
		std::print(" {:>{}},", mds[indices...], FieldWidth);
	}
	else
	{
		const auto is_innermost = (sizeof...(indices) == mds.rank() - 1);
		const auto indent = std::format("{:>{}}", "", sizeof...(indices));

		std::print("{}[{}", indent, is_innermost ? "" : "\n");
		for(auto i = 0; i < mds.extent(sizeof...(indices)); i++)
		{
			print(mds, indices..., i);
		}
		std::println("{}],", is_innermost ? " " : indent);
	}
}


// Prints the contents of a container
void
print(std::ranges::contiguous_range auto c)
{
	std::print("[");
	for(auto i = 0; auto&& e : c)
	{
		std::print("{}{}", i++ == 0 ? " " : ", ", e);
	}
	std::println(" ]");
}


void
describe_and_print(MDSpan auto mds)
{
	describe(mds);
	print(mds);
	std::println("");
}


//
// Examples

void
matrix_2d()
{
	auto matrix_data = std::array{
		1, 2, 3, 4,
		11, 22, 33, 44,
		111, 222, 333, 444,
	};

	// 3 rows, 4 cols (dynamic)
	describe_and_print(
		std::mdspan{ matrix_data.data(), 3, 4 });

	// 2 rows, 6 cols (dynamic)
	describe_and_print(
		std::mdspan{ matrix_data.data(), 2, 6 });

	// 3 rows, 4 cols (static)
	describe_and_print(
		std::mdspan<int, std::extents<int, 3, 4>>{ matrix_data.data() });

	// 3 rows (dynamic), 4 cols (static)
	describe_and_print(
		std::mdspan<int, std::extents<int, std::dynamic_extent, 4>>{ matrix_data.data(), 3 });

	// 3 rows (static), 4 cols (dynamic)
	describe_and_print(
		std::mdspan<int, std::extents<int, 3, std::dynamic_extent>>{ matrix_data.data(), 4 });

	// Transposed: 4 rows, 3 cols
	describe_and_print(
		std::mdspan<int, std::extents<int, 4, 3>, std::layout_left>{ matrix_data.data() });
}


void
matrix_nd()
{
	// Extents for a 3D matrix with x=2, y=3, z=4 extents
	const auto matrix_extents = std::extents<int, 4, 3, 2>{};

	// Data for a row-major array:
	// [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 ]
	const auto data_row_major = [matrix_extents] {
		auto data = std::vector<int>(fwd_prod_of_extents(matrix_extents));
		//std::ranges::iota(data, 0);	// New in C++23 but unavailable in libc++17
		std::iota(std::begin(data), std::end(data), 0);
		return data;
	}();

	// Row-major mdspan (rightmost extent has stride 1)
	const auto matrix_row_major = std::mdspan{ data_row_major.data(), matrix_extents };
	std::print("Row-major data: ");
	print(data_row_major);
	describe_and_print(matrix_row_major);

	// Let's compare this with a column-major mdspan backed by a vector.
	{
		// Create a column-major mdspan.
		// Note the remove_const_t. The C++ standard requires "Extents is a specialization of extents"
		// but it's not clear to me if that forbids a 'const extents'. Clang rejects it.
		auto data_col_major = std::vector<int>(fwd_prod_of_extents(matrix_extents));
		const auto matrix_col_major =
			std::mdspan<int, std::remove_const_t<decltype(matrix_extents)>, std::layout_left>{
				data_col_major.data()
			}
		;

		// Assign the row-major mdspan's elements one by one to the column-major mdspan
		for (auto z = 0; z < matrix_extents.extent(0); z++)
			for (auto y = 0; y < matrix_extents.extent(1); y++)
				for (auto x = 0; x < matrix_extents.extent(2); x++)
					matrix_col_major[z, y, x] = matrix_row_major[z, y, x];

		// Now compare the underlying data:
		// [ 0, 6, 12, 18, 2, 8, 14, 20, 4, 10, 16, 22, 1, 7, 13, 19, 3, 9, 15, 21, 5, 11, 17, 23 ]
		std::print("Column-major data (vector): ");
		print(data_col_major);
		describe_and_print(matrix_col_major);
	}

	// The same thing but with a multi-dimensional array to back the column-major mdspan.
	{
		int data_col_major[2][3][4] = {};
		// Note that std::mdspan does not currently have interoperability with C-style arrays but
		// P2554 attempts to solve it and may have been merged into C++26:
		// https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p2554r0.html
		// As a workaround we use a cast to flatten/decay the array.
		const auto matrix_col_major =
			std::mdspan<int, std::remove_const_t<decltype(matrix_extents)>, std::layout_left>{
				reinterpret_cast<int *>(data_col_major)
			};

		for (auto z = 0; z < matrix_extents.extent(0); z++)
			for (auto y = 0; y < matrix_extents.extent(1); y++)
				for (auto x = 0; x < matrix_extents.extent(2); x++)
					matrix_col_major[z, y, x] = matrix_row_major[z, y, x];

		// Now compare the underlying data:
		// [
		//  [
		//   [ 0, 6, 12, 18, ],
		//   [ 2, 8, 14, 20, ],
		//   [ 4, 10, 16, 22, ],
		//  ],
		//  [
		//   [ 1, 7, 13, 19, ],
		//   [ 3, 9, 15, 21, ],
		//   [ 5, 11, 17, 23, ],
		//  ],
		// ],
		std::print("Column-major data (multi-dimensional array): ");
		print_array(data_col_major);
		// Or cast to a 1D-array:
		//print_array(reinterpret_cast<const int(&)[md_array_size(data_col_major)]>(data_col_major));
		describe_and_print(matrix_col_major);
	}
}


// Note: libc++17 does not yet implement std::layout_stride. Use Clang 19 for this, e.g. on Wandbox.
void
custom_stride()
{
#if __clang_major__ >= 19
	{
		const auto data = std::array{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
		const auto mds = std::mdspan<const int, std::extents<size_t, 4, 3>>{ data.data() };
		describe_and_print(mds);

		// Extract specific columns
		{
			const auto extents = std::extents{ mds.extent(0) };
			const auto strides = std::array{ mds.stride(0) };
			const auto mapping = std::layout_stride::mapping{ extents, strides };
			const auto first_col = std::mdspan{ data.data(), mapping };
			describe_and_print(first_col);
			const auto second_col = std::mdspan{ data.data() + 1, mapping };
			describe_and_print(second_col);
		}

		// Extract the top-left 3x2 matrix
		{
			const auto extents = std::extents{ 3, 2 };
			const auto strides = std::array{ mds.stride(0), mds.stride(1) };
			const auto mapping = std::layout_stride::mapping{ extents, strides };
			const auto sub = std::mdspan{ data.data(), mapping };
			describe_and_print(sub);
		}
	}

	{
		// Pixel data for a 6x3 display but with each row aligned at an 8-word boundary
		constexpr auto Width  = 6uz;
		constexpr auto Height = 3uz;
		constexpr auto Stride = 8uz;
		const auto data = std::array<int, Height * Stride>{
			 0,  1,  2,  3,  4,  5, -1, -1,
			 6,  7,  8,  9, 10, 11, -1, -1,
			12, 13, 14, 15, 16, 17, -1, -1,
		};

		const auto data_view = std::mdspan<const int, std::extents<size_t, Height, Width>>{ data.data() };
		const auto extents = std::extents{ data_view.extent(0), data_view.extent(1) };
		const auto strides = std::array{ Stride, data_view.stride(1) };
		const auto mapping = std::layout_stride::mapping{ extents, strides };
		const auto pixel_view = std::mdspan{ data.data(), mapping };
		describe_and_print(pixel_view);
	}
#endif
}


// std::mdspan accessor that rotates the elements by 13.
// Note: Must only be used with mdspans that contain exactly 26 elements.
template<typename ElementType>
struct rot13_accessor
	: public std::default_accessor<ElementType>
{
	using base = std::default_accessor<ElementType>;

	constexpr base::reference access(base::data_handle_type p, size_t i) const noexcept
	{
		return base::access(p, (i + 13) % 26);	// Delegate to the base but with a tweaked index
	}
};


// std::mdspan accessor that rotates the elements by a given amount.
template<typename ElementType>
struct rot_accessor
	: public std::default_accessor<ElementType>
{
	using base = std::default_accessor<ElementType>;

	rot_accessor(size_t size, size_t rot)
		: size_{ size }
		, rot_{ rot }
	{}

	constexpr base::reference access(base::data_handle_type p, size_t i) const noexcept
	{
		return p[(i + rot_) % size_];	// Index the data directly
	}

private:
	size_t size_{};
	size_t rot_{};
};


void
custom_accessor()
{
	// Array: { 'A', 'B', 'C', … 'Z', '0', … '9' }
	const auto data = [] {
		auto data = std::array<char, 26 + 10>{};
		std::iota(std::begin(data), std::end(data), 'A');
		std::iota(std::begin(data) + 26, std::end(data), '0');
		return data;
	}();

	// Create a ROT13 view of only the 13x2 letters
	{
		const auto rot13_view = std::mdspan<
			const char,
			std::extents<size_t, 13, 2>,
			std::layout_right,
			rot13_accessor<const decltype(data)::value_type>	// Accessor fully defined by its type
		>{ data.data() };
		describe_and_print(rot13_view);
	}

	// Create a ROT2 view of the entire 6x6 data
	{
		auto accessor = rot_accessor<const char>{ data.size(), 2 };
		const auto rot_view = std::mdspan{
			data.data(),												// Data
			std::layout_right::mapping<std::extents<size_t, 6, 6>>{},	// Layout and extents
			accessor													// Accessor instance
		};
		describe_and_print(rot_view);
	}
}


int
main()
{
	matrix_2d();
	matrix_nd();
	custom_stride();
	custom_accessor();
}
